meta:
  title: '@:home.title ! #JoinPeerTube'
nav:
  langChange: Byt språk
  lang: Språk
  translate: Översätt
menu:
  faq: Vanliga frågor
  help: Support
  docs: Dokumentation
  code: Källkod
  instances: Instanser
  hall-of-fame: Hall of fame
link:
  forumPT: https://framacolibri.org/c/peertube
  wArticle: https://en.wikipedia.org/wiki
home:
  title: Återta kontrollen över dina videor
  intro:
    title: En decentraliserad videoplattform byggd på fri mjukvara
    getting-started: Get started
    how-it-works: Hur det funkar
  release:
    announce: PeerTube v1.0
    title: <a href="https://framablog.org/2018/10/15/peertube-1-0-the-free-libre-and-federated-video-platform/">Den
      första versionen av PeerTube är ute nu!</a> Hjälp oss fortsätta utvecklingen.
    button: Support
    install: Installera PeerTube
  why:
    power:
      title: Återta kontrollen … och ansvaret!
      desc: 'PeerTube är inte en enskild video-plattform med en uppsättning regler:
        det är ett nätverk av dussintals sammanlänkade instanser, alla med olika människor
        och administratörer. Gillar du inte någon av reglerna? Du är fri att välja
        vilken instans du vill, eller ännu bättre, starta en själv med dina egna regler!'
    content:
      title: Återta kontrollen över ditt innehåll
      desc: PeerTube låter dig dela alla dina videor. Direktkontakten med en mänsklig
        värd (eller att bli bli din egen) ger dig möjligheten att välja hur utsändningen
        hanteras. Dina videor kommer dra nytta av verktyg för att fylla i videobeskrivningar,
        kategorisering, välja förhandsvisningsbild och markera videor som olämpliga
        att se på jobbet. Genom att justera <strong>Support</strong>-knappen kan du
        visa din publik hur de kan stödja ditt arbete.
    usersfirst:
      title: Sätter användarna främst
      desc: 'Du är en människa, inte en produkt. PeerTube är en fri mjukvara finansierad
        av en fransk ideell förening: @:data.html.soft</>. Alla instanser skapas,
        drivs, modereras och underhåll oberoende av varandra. PeerTube står inte under
        ett företags monopol, är inte beroende av annonser och spårar inte heller
        dig. Med PeerTube är du inte en produkt: PeerTube står till din tjänst, inte
        tvärt om.'
    broadcast:
      title: Bli en aktör i din videodistribution
      desc: När du tittar på en video med PeerTube gör WebTorrent det möjligt att
        bli en del av utsändningen av videon till andra som ser på videon samtidigt.
        Den här delningen av videoströmmen ger en bättre distribution av noder över
        nätverket. Dessutom tillåter federationsprotokollet (ActivityPub) publicering
        och kommentarer via andra plattformar som stöder det, som till exempel <a
        href="@:data.link.mastodon">Mastodon</a>! (experimentellt)
  getting-started:
    title: Get started
    watch:
      title: Titta
      framatube: Se videor på @:data.color.tube
    register:
      title: Registrera
      list: 'Lista över instanser du kan registrera ett konto på:'
      error: Vi kan tyvärr inte ladda listan över tillgängliga instanser. Försök gärna
        igen senare.
      email: Det är lite som att välja en e-postleverantör, domänen blir en del av
        ditt användarnamn!
      instances:
        per_user: per användare
        followers: följare
        instances: instanser
        follows: följer
        bytes:
          B: B
          KB: kB
          MB: MB
          GB: GB
        no_quota: No quota
  install:
    title: Installera din egen
    text:
    - Om du skulle vara intresserad av att driva din egen instans – för dina vänner,
      familj eller organisation – kan du komma igång genom att <a href="@:data.link.gitPT/blob/develop/support/doc/production.md">läsa
      insallationsdokumentationen</a>.
    - Du kommer endast stå värd för dina egna användare och deras videor. Du kan välja
      hur många användare som kan registrera sig och hur mycket lagringsutrymme varje
      användare får. Bara videor från instanser <strong>du valt att följa</strong>
      kommer synas på din hemsida.
    btn: Läs dokumentationen
  how-it-works:
    how:
      title: How it works
      text:
      - Vem som helst kan skaffa en PeerTube-server, vad vi kallar en <strong>instans</strong>.
        Varje instans är värd för sina användare och deras videor. Den sparar också
        en lista över videor som gjorts tillgängliga på de instanser administratören
        valt att följa, så att den kan föreslå även dem till sina användare.
      - Varje konto har en globalt unik identifierare (till exempel @chocobozzz@framatube.org)
        bestående av det lokala användarnamnet (@chocobozzz) och dess servers domännamn
        (framatube.org).
      - PeerTube-instansernas administratörer kan följa varandra. När din PeerTube-instans
        följer en annan kan du ta emot information om den andra instansens videor.
        På så sätt kan du visa videorna från både din instans och den du valt att
        följa. Du har alltså full kontroll över vilka videor som visas på din PeerTube-instans!
      btn: Frågor?
    why:
      title: Varför är det häftigt?
      text:
      - Servrarna drivs oberoende av varandra av olika personer och organisationer.
        De kan tillämpa vitt skilda moderationspolicyer så att du kan finna eller
        skapa en som passar dig perfekt.
      - När du tittar på en video hjälper du servern att förmedla den, genom att själv
        skicka den vidare. På så vis behöver varje enskild instans inte någon större
        budget för att kunna stå värd för och sända ut sina användares videor.
      btn: Get started
footer:
  text: Baserat på
  thanks: Tack!
faq:
  title: Några frågor för att upptäcka PeerTube …
  clic: (klicka på en fråga för att se svaret)
  section:
    prez: Presentation av PeerTube
    content: Skapande och innehåll
    tech: Tekniska frågor
  prez:
    what:
      title: Vad är PeerTube?
      text:
      - PeerTube är mjukvara som kan installeras på en webbserver för att skapa en
        videodelnings-webbplats, ett ”hemmabyggt YouTube”.
      - Till skillnad från YouTube är tanken inte skapa en gigantisk plattform som
        samlar videor från hela världen på en serverfarm (vilket är fruktansvärt dyrt).
      - I stället är PeerTubes koncept att skapa ett nätverk av sammanlänkade små
        värdservrar.
    pros:
      title: De tre största fördelarna med PeerTube.
      text:
      - 'PeerTubeär unikt eftersom (så vitt vi vet) är den enda videoplattformen som
        kombinerar de följande tre fördelarna:'
      - Tillsammans gör de tre finesserna det enkelt att driva delningsservern och
        gör det samtidigt praktiskt, etiskt och roligt för internetanvändarna.
      list:
      - Öppen källkod (transparens) under en fri / libre-licens (etisk och respektfull
        utveckling driven av en gemenskap);
      - En federation av sammanlänkade värdar (vilket ger fler videor att välja mellan
        var du än väljer att se dem);
      - Videodelning och -tittande med P2P (så det går inte långsammare om en video
        skulle bli väldigt populär).
    libre:
      title: Varför är det bättre som fri / libre-mjukvara?
      text:
      - Eftersom genom att skapa fri mjukvara respekteras våra grundläggande friheter,
        vilket garanteras genom <a href="@:data.link.gitPT/blob/develop/LICENSE">en
        licens</a>, alltså ett juridiskt bindande kontrakt.
      - 'Konkret betyder det att:'
      list:
      - PeerTube är gratis, du behöver inte betala för att installera det på din server;
      - 'Vi kan se hur PeerTube fungerar bakom kulisserna (dess källkod): det går
        att granska och är transparent;'
      - Dess utveckling är baserat på en gemenskap och kan förbättras av allas bidrag.
    federated:
      title: Vad är vitsen med att federera videovärdarna?
      text:
      - 'Fördelen med YouTube (och andra plattformar) är deras videokatalog: från
        instruktionsfilmer om stickning till Minecraft-konstruktioner och kattungar
        eller semesterfilmer … du kan hitta precis vad som helst!'
      - Ju mer variation i videokatalogen, desto fler blir intresserade och desto
        fler videor laddas upp … men att tillhandahålla all världens videor blir väldigt
        dyrt!
      - Om administratören för Knitting-PeerTube blir vän med Kittens-Tube och Framatube
        kommer deras videor visas på Knitting-PeerTube. På så sätt späs driftkostnaderna
        ut och det förblir praktiskt och komplett för internetanvändare.
      - 'PeerTubes federationsprotokoll kommer vara anpassningsbart (alla kan välja
        sina ”väninstanser”) och baserat på <a href="@:data.link.activitypub">ActivityPub</a>:
        detta kommer öppna för möjligheten att ansluta med verktyg som Mastodon eller
        MediaGoblin.'
    p2p:
      title: Varför skicka PeerTube-videor över P2P?
      text:
      - 'När du tillhandahåller är en stor fil, som en video, är framgång det som
        är mest skrämmande: om en video blir väldigt populär och många ser på den
        samtidigt löper servern en stor risk att bli överbelastad!'
      - Förmedling från person till person med <a href="@:link.wArticle/WebRTC">WebRTC</a>
        tillåter Internetanvändare som tittar på samma video dela med sig av delar
        av filer, vilket avlastar servern server.
      - 'Det finns inget du behöver göra: din webbläsare gör det automatiskt. Om du
        använder en mobiltelefon eller om ditt nätverk inte tillåter det (router,
        brandvägg etc.) kommer funktionen inte avaktiveras och övergå till ”gammaldags”
        video-sändning. @:data.emoji.wink</>'
    admin:
      title: För de som vet hur man administrerar en server, är PeerTube …
      text:
      - '<strong>Det är programvara du installerar på din server</strong> för att
        skapa en webbplats där videor sparas och sänds ifrån. Alltså: du skapar ditt
        eget ”hemmabyggda YouTube”!'
      - Det finns redan fri mjukvara som låter dig göra detta men med PeerTube kan
        du länka din instans (din video-webbplats) till Zaïds PeerTube-instance (där
        han har videor från föreläsningarna på sitt universitet), till Catherins (som
        lägger upp videor om webbmedia) eller till Solars PeerTube-instans (som samordnar
        en grupp vloggare).
      - <strong>Men PeerTube centraliserar inte, det federerar.</strong> Tack vare
        <a href="@:data.link.activitypub">ActivityPub-protokollet</a> (som också används
        av <a href="@:data.link.mastodon">Mastodon-federationen</a>, ett fritt alternativ
        till Twitter) kan PeerTube federera många små värdar så att de inte måste
        köpa tusentals hårddiskar för att lagra videor från hela världen.
      - Det gör att publiken inte bara kan se dina videor på din instans, utan även
        videor som lagras hos Zaïd, Catherin eller Solar utan att behöva spara deras
        videor på din PeerTube-webbplats. Sådan bredd i videokatalogen gör det väldigt
        tilltalande. Det var den stora valmöjligheten och mångfalden i videoutbudet
        som gjorde centraliserade plattformar som YouTube framgångsrika.
      - 'Federation har också andra fördelar: <strong>alla blir oberoende</strong>.
        Zaïd, Catherin, Solar och du själv skriva era egna regler, era egna användarvillkor
        (till exempel kan MeowTube totalförbjuda hundvideor @:data.emoji.wink).'
    video-maker:
      title: För de som vill ladda upp sina videor tillåter PeerTube …
      text:
      - 'Det låter dig hitta en värd som passar dig. YouTubes överträdelser är ett
        tydligt exempel: YouTubes ägare, Google och Alphabet, kan påtvinga sin ”Robocopyright”
        (ContentID-systemet) samt sina egna verktyg för att indexera, rekommendera
        och belysa videor; och de verktygen verkar vara lika orättvisa som de är obskyra.
        Detta trots att de redan tvingar dig <a href="@:data.link.tosdr">att ge dem
        utökad upphovsrätt till dina videor, utan ersättning</a>!'
      - Med PeerTube <strong>kan du välja vem som står värd för dina videor med hänsyn
        till vederbörandes användarvillkor</strong>, moderationspolicy, federationsval
        och mycket mer. Eftersom du inte står öga mot öga med en teknikjätte, har
        möjlighet att få tag i din värd om du skulle stöta på ett problem, behov eller
        önskemål.
      - En annan stor fördel PeerTube har är att din värd inte behöver frukta att
        någon av dina videor plötsligt blir populär. PeerTube delar videor med protokollet
        <a href="@:link.wArticle/BitTorrent">WebTorrent</a>. Om flera hundra personer
        tittar på din video samtidigt kommer deras webbläsare automatiskt skicka vidare
        bitar av videon till andra tittare.
      - Innan P2P-sändningar, var framgångsrika videoskapare (och omtalade videor)
        tvungna att delas på gigantiska webbplattformar, vars infrastruktur kan hantera
        flera miljoner samtidiga visningar eller betala en väldigt dyr oberoende videotjänst
        så att den kan klara belastningen.
    audience:
      title: För de som vill titta på videor, erbjuder PeerTube …
      text:
      - En av fördelarna är att <strong>du blir en del av utsändningen av videon du
        tittar på</strong>. Om andra ser på en PeerTube-video samtidigt som du, delar
        din webbläsare delar av videon tills fliken stängs, och du bidrar till en
        sundare internetanvändning.
      - 'Självfallet anpassar sig PeerTubes videospelare till din situation: om din
        installation inte tillåter P2P-strömning (företagsnätverk, motsträvig webbläsare,
        etc.) kommer videon skickas på det gamla beprövade viset.'
      - Men framför allt <strong>behandlar PeerTube dig som en människa, inte en produkt</strong>
        som måste spåras, profileras och stängas in i video-loopar för att sälja din
        tid mer effektivt. Därför är <a href="@:data.link.gitPT">källkoden</a> (kvittot)
        för PeerTube öppen, vilket gör mjukvarans beteende transparent.
      - '<strong>PeerTube har inte bara öppen källkod: det är fritt.</strong> Dess
        fria licens garanterar våra grundläggande användarfriheter. Det är den vördnaden
        för vår frihet som gör att Framasoft inbjuder dig att bidra till den här mjukvaran
        och många förbättringar (bland annat ett innovativt system för kommentarer)
        har redan föreslagits av några av er.'
    remplace-yt:
      title: Är PeerTube tänkt att ersätta YouTube?
      text:
      - 'På det kan vi svara med säkerhet: nej!'
      - I mars 2018 släppte PeerTube sin första publikt användbara beta-version. Ett
        flertal gemenskaper satte upp sina första instanser, vilket lade grunden för
        federationen.
      - But this is just the beginning, PeerTube is not (yet) perfect, and many features
        are missing. But we intend to keep improving it day after day.
      - 'Mars 2018 blev alltså startskottet för PeerTube-federationen: ju mer den
        här programvaran kommer användas och stödjas, desto fler kommer använda och
        bidra till den och den kommer då utvecklas allt snabbare till att bli ett
        stabilt alternativ till plattformar som YouTube.'
      - 'Nåväl, ambitionen kommer även i fortsättningen att vara <strong>ett fritt
        och decentraliserat alternativ</strong>: ett alternativs mål är inte att ersätta,
        utan att föreslå någonting annat, med andra värden, parallellt med det som
        redan existerar.'
  content:
    law:
      title: Om det är fritt, kan vi då ladda upp olagliga saker där?
      text:
      - Att vara fri innebär inte att stå över lagen! Varje PeerTube-värd kan bestämma
        sina egna allmänna användarvillkor utifrån sin lokala lagstiftning.
      - Till exempel är kränkande innehåll <a href="https://fr.wikipedia.org/wiki/Lois_contre_le_racisme_et_les_discours_de_haine">förbjudet
        i Frankrike</a> och kan <a href="http://stop-discrimination.gouv.fr/agir/ne-pas-laisser-faire-les-recours">anmälas
        till myndigheterna</a>. PeerTube tillåter användare att rapportera problematiska
        videor och varje administratör måste då tillämpa moderation i enighet med
        sina villkor och lagen.
      - Federationssystemet låter värdarna avgöra vilka de vill ansluta till, beroende
        på den andra partens typ av innehåll eller moderationspolicy.
    responsible:
      title: Vem ansvarar för innehåll som publiceras på PeerTube?
      text:
      - 'PeerTube är inte en webbplats: det är programvara som låter en webb-värd
        (till exempel Dominique) skapa en webbplats för videodelning (som vi kan kalla
        DominiqueTube).'
      - Om nu Camille har skaffat sig ett konto på DominiqueTube och laddar upp en
        video som är olaglig då den innehåller musik skapad Solal.
      - Solal besöker Framatube, en instans som följer DominiqueTube. Solal kan då
        se videon som publicerats på DominiqueTube från Framatube.
      - Solal ser Camilles olagliga video och signalerar det med knappen som finns
        för det ändamålet. Även om rapporteringen gjordes från Framatube, skickas
        den direkt till personen vars server tillhandahåller det illegala innehållet,
        Dominique.
      - Från det ögonblicket är Dominique ansvarig eftersom han har varnats om att
        han delar en illegal video. Det är då upp till honom att agera för att inte
        ställas ansvarig inför lagen.
      - Då kan Dominique och Solal vända sig emot Camille som laddade upp videon.
    money:
      title: Vad är PeerTubes ersättningspolicy?
      text:
      - OrderedDict([('Det finns ingen för tillfället', 'PeerTube är ett verktyg som
        vi vill göra naturligt i fråga om ersättningar.')])
      - 'För tillfället är lösningen vi ger till de som laddar upp videor att använda
        ”support”-knappen under videon. Knappen visar en ruta där de som laddar upp
        videor kan visa text, bilder och länkar helt fritt. Det är till exempel möjligt
        att lägga in en länk till Patreon, Tipeee, Paypal, Liberapay (eller vilken
        annan lösning som helst) där. Andra exempel: lägg in en postadress dit folk
        kan skicka fysiska tackkort, ditt företags logotyp, en länk för att stöda
        en ideell förening …'
      - Vi gick inte längre än så för att främja någon teknisk lösning eftersom det
        skulle innebära att, i koden, påtvinga en politisk vision för kulturell delning
        och dess finansiering. Alla finansiella lösningar är möjliga och behandlas
        likvärdigt av PeerTube.
      - Däremot väntas många förbättringar av PeerTube, bland annat några som låter
        dig skapa (och välja) de verktyg för intäktsgenerering som intresserar dig!
      - 'Hur som helst kan det vara bra att komma ihåg att de flesta videor som publiceras
        på Internet (även på YouTube) har delats för icke-kommersiella ändamål: ersättningar
        är ett verktyg, men inte nödvändigtvis ett huvudsakligt eller oersättligt
        syfte.'
    instances:
      title: Var kan jag spara mina videor?
      text:
      - Du måste hitta en PeerTube-instans du kan lita på.
      - Det finns en komplett <a href="@:data.link.instancesPT">lista över instanser
        här</a> och en lista över de som <a href="./#getting-started">tillåter registrering
        här</a>.
      - Därefter rekommenderar vi att du går till instansen och läser deras informationssida
        för att se instansens användarvillkor (begränsningar rörande diskutrymme,
        innehållspolicy och liknande).
      - Det är bäst att kontakta värden direkt för att ta reda på vederbörandes affärsmodell
        och vision. Detta eftersom endast du vet vad som får dig lita på en värd och
        vilken sorts värd du vill anförtro dina videor åt.
    pornography:
      title: Det finns mycket pornografi på PeerTube!
      text:
      - No. In October 2018, on an average instance federating with ~200 instances
        and indexing ~16000 videos, only ~200 videos are tagged as NSFW (i. e. the
        content is sensitive, which could be something else than pornography). Therefore,
        they represent only ~1% of all the videos.
      - 'Moreover, each administrator decides with which instances he wants to federate:
        he has the full control of the content he wants to display on his instance.
        It''s up to him to choose the policy regarding this kind of videos. He can
        decide to: <ul><li>Display them</li><li>Blur the title and thumbnail</li><li>Hide
        them</li></ul>'
      - By default, this configuration is set to "Hide them". If some administrators
        decide to display them with a blur filter for example, it's <strong>their</strong>
        choice.
      - Finally, any user can override this configuration, and decides if he want
        to display, blur or hide these videos for himself.
      - 'PeerTube is just a software: it''s not Framasoft (non-profit that develops
        PeerTube) that''s responsible for the content published on some instances.'
      - 'Det är upp till var och en att agera ansvarsfullt: föräldrar, besökare, uppladdare
        och PeerTube-administratörer att respektera lagar och undvika problematiska
        situationer.'
  forum: Diskutera på vårt forum
  tech:
    install:
      title: Hur installerar jag PeerTube?
      text:
      - <a href="@:data.link.gitPT/blob/develop/support/doc/production.md">Installationsguiden
        finns här</a> (endast på engelska för tillfället).
      - Vi avråder från att installera PeerTube på hårdvara av lägre kvalitet eller
        med en långsam internetanslutning (som en RaspberryPi med ADSL-uppkoppling)
        då det kan göra federationen långsammare.
      - Stör inte utvecklarna om du behöver hjälp att installera din instans – vi
        har ett <a href="@:link.forumPT">hjälpforum</a> för sådana frågor.
    moderation:
      title: PeerTube version 1.0 verkar inte tillhandahålla alla de verktyg jag behöver
        för att hantera min instans på ett bra sätt.
      text:
      - '<blockquote>”Det är djupt upprörande och vettlöst: ni släpper version ett
        av PeerTube 1.0 när det inte har de verktyg som krävs för att effektivt hantera
        videor med krav från upphovsrättsinnehavare, eller på ett slagkraftigt sätt
        hantera problemet med trakasserier i kommentarer, eller för att effektivt
        hantera intäktsgenerering  från annonser, eller [skriv ditt krav på PeerTube
        här]. Det kommer aldrig fungera! Vad tänker ni göra åt det?”</blockquote>'
      - Du har rätt. PeerTube 1.0 är inte det perfekta verktyget, långt därifrån.
        Vi har aldrig lovat att version 1.0 skulle vara ett verktyg med samtliga funktioner
        för alla fall.
      - 'PeerTube 1.0 är förverkligandet av åtagandet vi tog på oss i oktober 2017:
        att ta PeerTube från en alfa-version (ett personligt projekt och bevis på
        att konceptet med en federerad videoplattform kan fungera) till version 1.0  i
        oktober 2018 (vilket inte innebär slutgiltig version, utan en version som
        kan anses stabil och klar för distribution).'
      - Kom ihåg att PeerTube endast har en utvecklare nästan på heltid och en handfull
        mycket engagerade volontärer. Det är inte en produkt utvecklad av ett uppstartsföretag
        med ett team anställt på heltid (utveckling, design, användargränssnitt, marknadsföring,
        support, etc.) och ordentligt finansiellt stöd. Det är en gemenskaps fria
        mjukvara vars utveckling kommer fortlöpa under många månader och, förhoppningsvis,
        år framöver.
      - 'Vi är väl medvetna tillkortakommanden i version 1.0 av PeerTube, speciellt
        när det kommer till moderationsverktyg (videor, kommentarer etc.) och vi tänker
        jobba på de svagheterna. '
      - 'Vi har valt att göra såhär: vi kommer huvudsakligen arbeta med att förbättra
        dessa verktyg i PeerTube (i mjukvarans <i>kärna</i>). Vi kommer även, parallellt
        med detta, lägga en stor del av PeerTubes utvecklingskraft under 2019 på integrationen
        av ett system för insticksprogram, vilka kan utvecklas av gemenskapen.'
      - Mycket riktigt; vi anser oss inte vara experter på området eller veta hur
        man på bästa sätt använder verktygen i alla fall. I frågor rörande upphovsrätt,
        till exempel, varierar fallen mycket mellan olika geografiska och juridiska
        områden (EU-lagstiftningen skiljer sig från den franska, som i sin tur är
        mycket olik den kanadensiska). Angående moderationsverktyg för kommentarer,
        kan vi inte heller där förklara oss experter eftersom så helt enkelt inte
        är fallet.
      - Genom att både vidta åtgärder i <i>kärnan</i> och möjliggöra utvecklingen
        av insticksprogram, tror vi att PeerTube på sikt kommer kunna hantera sådana
        problem mycket bättre och låta olika gemenskaper anpassa PeerTube till sina
        behov.
      - Vi arbetar så fort som möjligt på att förbättra PeerTube, men vi gör det med
        de resurser vi har, vilka är <strong>mycket</strong> begränsade.
      - 'Tills dess, om du som användare upplever att PeerTube 1.0 inte möter dina
        behov för tillfället finns det en enkel lösning: använd det inte just nu (vi
        vill påminna om att vi inte tjänar pengar på att utveckla PeerTube och att
        vi naturligtvis hoppas på dess framgång, men vår förenings överlevnad hänger
        inte på det).'
      - Det är möjligt att begränsa kontoregistrering till personer du känner om du
        som administratör oroar dig för upphovsrättsliga krav. Du kan sedan öppna
        upp för registrering utan verifikationskrav när dessa verktyg har integrerats,
        eller du har utvecklat dem själv.
    code:
      title: Hur kan jag bidra till PeerTubes kod?
      text:
      - PeerTubes <a href="@:data.link.gitPT">Git-repository finns här</a>.
      - Du kan <a href="@:data.link.gitPT/issues">skriva ett förbättringsärende</a>,
        bidra till koden eller börja med att välja något av de <a href="@:data.link.gitPT/issues?q=is%3Aissue+is%3Aopen+label%3A%22good+first+issue%22">problem
        som är enkla att börja med</a>.
      - Om du vill hjälpa till på något annat sätt eller vill föreslå en ny funktion
        eller finess, kom gärna och diskutera det på vårt <a href="@:link.forumPT">forum
        för medhjälpare</a>.
    protocol:
      title: Varför använder PeerTube federationsprotokollet ActivityPub och inte
        IPFS, d.tube eller Steemit?
      text:
      - PeerTube använder ActivityPub eftersom det federationsprotokollet rekommenderas
        av W3C och redan används av det federerade sociala nätverket Mastodon.
      - IPFS är en fantastisk teknik, men verkar fortfarande vara för nytt för storskalig
        strömning av stora filer.
      - Efter att ha diskuterat detta på vårt forum, kom vi fram till att d.tube inte
        är fri programvara eller har öppen källkod eftersom publiceringen av kompilerad
        kod hindrar friheten att modifiera.
      - D.tubea system för ”ersättningar” är baserat på Steem, det är ett val, men
        Steem är <a href="https://en.wikipedia.org/wiki/Steemit#Criticism">starkt
        kritiserat</a> för att vara <a href="https://steemit.com/steemit/@docdelux/30-days-of-steemit-here-is-my-criticism">mycket
        centraliserat</a> och misstänkt <a href="https://steemit.com/steemit/@thecryptonews/psa-constructive-criticism-the-ugly-truth-behind-steemit">likt
        ett ponzibedrägeri</a>.
      - PeerTube är fritt, decentraliserat, utspritt och tillhandahåller inte någon
        avlöningsmodell. Detta är ett val vi gjort, som går att diskutera, och andra
        (som d.tube) har gjort andra val som även de har sina fördelar. Så det är
        upp till dig att välja vad som passar dig.
hof:
  title: Hall of fame
  sponsors: Sponsorer
  donators: Finansiella bidragsgivare
  dev: Bidragsgivare
  contrib: Hjälp till med koden
